import ufl
from ufl import TrialFunction, TestFunction, Identity, sym, grad, dx,\
    nabla_div, inner, SpatialCoordinate, dot, Constant, sin, cos, action
from dolfinx import fem, mesh, plot, io
import numpy as np
from mpi4py import MPI
from petsc4py.PETSc import ScalarType
from petsc4py import PETSc
import sys

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.size
pi = 3.14159265359
# Define Properties#####################################################################################################

i = int(sys.argv[1])
multiplier = int(sys.argv[2])


t1 =MPI.Wtime()
L1,L2,L3 = 20,20,20
# multiplier = 10
nelx = 20 * multiplier                         # Number of elements in the x-direction
nely = 20 * multiplier                          # Number of elements in the y-direction
nelz = 20 * multiplier                           # Number of elements in the z-direction
nelem = nelx * nely * nelz                   # Total number of elements


msh = mesh.create_box(comm=comm,
                            points=((0, 0, 0), (L1, L2, L3)),
                            n=(nelx, nely, nelz),
                            cell_type=mesh.CellType.hexahedron)

V = fem.FunctionSpace(msh, ("CG", 1))
# if rank==0:
#     print("Done Meshing\n")

def faces(x):
    tol = 1e-6
    return np.any(np.array([
        np.abs(x[0]) < tol,
        np.abs(x[1]) < tol,
        np.abs(x[2]) < tol,
        np.abs(x[0] - L1) < tol,
        np.abs(x[1] - L2) < tol,
        np.abs(x[2] - L3) < tol
    ]), axis= 0)


facets = mesh.locate_entities_boundary(msh, 2, faces)
dofs = fem.locate_dofs_topological(V, 2, facets)
bc = fem.dirichletbc(ScalarType(0.0), dofs, V)
x = SpatialCoordinate(msh)
W = fem.FunctionSpace(msh, ("DG", 0))
# f = fem.Function(V)
f = pi*pi*(1/L1/L1 + 1/L2/L2 + 1/L3/L3) * sin(x[0]*pi/L1)*sin(x[1]*pi/L2)*sin(x[2]*pi/L3)
u = TrialFunction(V)
v = TestFunction(V)

a = dot(grad(u), grad(v)) * dx
L = f * v * dx
L_fem = fem.form(L)
problem = fem.petsc.LinearProblem(a, L, bcs=[bc], petsc_options={"ksp_type": "cg", "pc":"ilu"})
t2 = MPI.Wtime()
uh = problem.solve()
t3 = MPI.Wtime()
u = fem.Function(V)
u.interpolate(lambda x:  np.sin(x[0]*np.pi/L1)*np.sin(x[1]*np.pi/L2)*np.sin(x[2]*np.pi/L3))
error = (np.linalg.norm(u.x.array[:]- uh.x.array[:]))

total_t=(t3-t1)
solver_t=(t3-t2)


u = fem.Function(V)
u.interpolate(lambda x:  np.sin(x[0]*np.pi/L1)*np.sin(x[1]*np.pi/L2)*np.sin(x[2]*np.pi/L3))
error=np.linalg.norm(u.x.array[:]- uh.x.array[:])
if rank == 0:
    print(f"{size}"
          f",\t{multiplier}"
          f",\t{solver_t}"
          f",\t{total_t}"
          f",\t{error}"
          )
    with open('output.txt', 'a') as f:
        f.write(f"{size}"
          f",\t{multiplier}"      
          f",\t{solver_t}"
          f",\t{total_t}"
          f",\t{error}\n"
          )
