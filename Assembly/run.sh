export OMP_NUM_THREADS=1
for i in 1 2 4 6 8
do
  for j in 4 8 10 12 14 16 18
  do
    mpirun -np $i python3 Heat.py $i $j
  done
done
