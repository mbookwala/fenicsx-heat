import ufl
from ufl import TrialFunction, TestFunction, Identity, sym, grad, dx,\
    nabla_div, inner, SpatialCoordinate, dot, Constant, sin, cos, action
from dolfinx import fem, mesh, plot, io
import numpy as np
from mpi4py import MPI
from petsc4py.PETSc import ScalarType
from petsc4py import PETSc
import sys

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.size
pi = 3.14159265359
# Define Properties#####################################################################################################

i = int(sys.argv[1])
multiplier = int(sys.argv[2])


t1 =MPI.Wtime()
L1,L2,L3 = 20,20,20
# multiplier = 10
nelx = 20 * multiplier                         # Number of elements in the x-direction
nely = 20 * multiplier                          # Number of elements in the y-direction
nelz = 20 * multiplier                           # Number of elements in the z-direction
nelem = nelx * nely * nelz                   # Total number of elements

# if rank == 0:
#     print(f"\nMultiplier = {multiplier}"
#           f",\t NELEM = {nelem}")
#
#     with open('output.txt', 'a') as f:
#         f.write(f"\nMultiplier = {multiplier}"
#           f",\t NELEM = {nelem}")

msh = mesh.create_box(comm=comm,
                            points=((0, 0, 0), (L1, L2, L3)),
                            n=(nelx, nely, nelz),
                            cell_type=mesh.CellType.hexahedron)

V = fem.FunctionSpace(msh, ("CG", 1))
# if rank==0:
#     print("Done Meshing\n")

def faces(x):
    tol = 1e-6
    return np.any(np.array([
        np.abs(x[0]) < tol,
        np.abs(x[1]) < tol,
        np.abs(x[2]) < tol,
        np.abs(x[0] - L1) < tol,
        np.abs(x[1] - L2) < tol,
        np.abs(x[2] - L3) < tol
    ]), axis= 0)


facets = mesh.locate_entities_boundary(msh, 2, faces)
dofs = fem.locate_dofs_topological(V, 2, facets)
bc = fem.dirichletbc(ScalarType(0.0), dofs, V)
x = SpatialCoordinate(msh)
W = fem.FunctionSpace(msh, ("DG", 0))
# f = fem.Function(V)
f = pi*pi*(1/L1/L1 + 1/L2/L2 + 1/L3/L3) * sin(x[0]*pi/L1)*sin(x[1]*pi/L2)*sin(x[2]*pi/L3)
u = TrialFunction(V)
v = TestFunction(V)

a = dot(grad(u), grad(v)) * dx
L = f * v * dx
L_fem = fem.form(L)
# problem = fem.petsc.LinearProblem(a, L, bcs=[bc], petsc_options={"ksp_type": "cg", "pc":"ilu"})
# t2 = MPI.Wtime()
# uh = problem.solve()
# t3 = MPI.Wtime()
# u = fem.Function(V)
# u.interpolate(lambda x:  np.sin(x[0]*np.pi/L1)*np.sin(x[1]*np.pi/L2)*np.sin(x[2]*np.pi/L3))
# error = (np.linalg.norm(u.x.array[:]- uh.x.array[:]))

# total_t=(t3-t1)
# solver_t=(t3-t2)
rtol = 1e-6
max_iter = 200
# ################# matrix free ##########################
t2 = MPI.Wtime()
ui = fem.Function(V)
M = action(a, ui)
M_fem = fem.form(M)

b = fem.petsc.assemble_vector(L_fem)
# Apply lifting: b <- b - A * x_bc
fem.set_bc(ui.x.array, [bc], scale=-1)
fem.petsc.assemble_vector(b, M_fem)
b.ghostUpdate(addv=PETSc.InsertMode.ADD, mode=PETSc.ScatterMode.REVERSE)
fem.petsc.set_bc(b, [bc], scale=0.0)
b.ghostUpdate(addv=PETSc.InsertMode.INSERT, mode=PETSc.ScatterMode.FORWARD)


def action_A(x):
    # Update coefficient ui of the linear form M
    x.copy(ui.vector)
    ui.x.scatter_forward()

    # Compute action of A on x using the linear form M
    y = fem.petsc.assemble_vector(M_fem)

    # Set BC dofs to zero (effectively zeroes rows of A)
    with y.localForm() as y_local:
        fem.set_bc(y_local, [bc], scale=0.0)
    y.ghostUpdate(addv=PETSc.InsertMode.ADD,
                  mode=PETSc.ScatterMode.REVERSE)
    return y


class Poisson:
    def create(self, A):
        M, N = A.getSize()
        assert M == N

    def mult(self, A, x, y):
        action_A(x).copy(y)

A = PETSc.Mat()
A.create(comm=msh.comm)
A.setSizes(((b.local_size, PETSc.DETERMINE),
            (b.local_size, PETSc.DETERMINE)), bsize=1)
A.setType(PETSc.Mat.Type.PYTHON)
A.setPythonContext(Poisson())
A.setUp()

solver = PETSc.KSP().create(msh.comm)
solver.setOperators(A)
solver.setType(PETSc.KSP.Type.CG)
solver.getPC().setType(PETSc.PC.Type.NONE)
solver.setTolerances(rtol=rtol, max_it=max_iter)
solver.setConvergenceHistory()

def converged(ksp, iter, r_norm):
    rtol, _, _, max_iter = ksp.getTolerances()
    if iter > max_iter:
        return PETSc.KSP.ConvergedReason.DIVERGED_MAX_IT
    r0_norm = ksp.getConvergenceHistory()[0]
    if r_norm / r0_norm < rtol:
        return PETSc.KSP.ConvergedReason.CONVERGED_RTOL
    return PETSc.KSP.ConvergedReason.ITERATING


solver.setConvergenceTest(converged)

uh_cg2 = fem.Function(V)
solver.solve(b, uh_cg2.vector)

# Set BC values in the solution vectors
uh_cg2.x.scatter_forward()
with uh_cg2.vector.localForm() as y_local:
    fem.set_bc(y_local, [bc], scale=1.0)

u = fem.Function(V)
u.interpolate(lambda x:  np.sin(x[0]*np.pi/L1)*np.sin(x[1]*np.pi/L2)*np.sin(x[2]*np.pi/L3))
error=np.linalg.norm(u.x.array[:]- uh_cg2.x.array[:])
t3 = MPI.Wtime()
total_t=(t3-t1)
solver_t=(t3-t2)
if rank == 0:
    print(f"{size}"
          f",\t{multiplier}"
          f",\t{solver_t}"
          f",\t{total_t}"
          f",\t{error}"
          )
    with open('output.txt', 'a') as f:
        f.write(f"{size}"
          f",\t{multiplier}"      
          f",\t{solver_t}"
          f",\t{total_t}"
          f",\t{error}\n"
          )
    # with open("total_time.csv","ab") as f:
    #     f.write(b"")
    #     np.savetxt(f, [total_t])
    # with open("solver_time.csv","ab") as f:
    #     f.write(b"")
    #     np.savetxt(f, [solver_t])
    # with open("error.csv","ab") as f:
    #     f.write(b"")
    #     np.savetxt(f, error)